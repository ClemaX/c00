/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putnbr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/01 14:30:26 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/01 15:54:28 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

void	ft_nextchar(unsigned int nb)
{
	char r;

	if (nb != 0)
	{
		r = (nb % 10) + '0';
		nb = nb / 10;
		ft_nextchar(nb);
		write(1, &r, 1);
	}
}

void	ft_putnbr(int nb)
{
	unsigned int u_nb;

	u_nb = nb;
	if (nb < 0)
	{
		u_nb *= -1;
		write(1, "-", 1);
	}
	else if (nb == 0)
		write(1, "0", 1);
	ft_nextchar(u_nb);
}
