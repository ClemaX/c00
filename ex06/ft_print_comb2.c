/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_comb2.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/01 15:23:02 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/01 16:19:42 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

void	ft_nextchar(unsigned int nb)
{
	char r;

	if (nb != 0)
	{
		r = (nb % 10) + '0';
		nb = nb / 10;
		ft_nextchar(nb);
		write(1, &r, 1);
	}
}

void	ft_putnbr2(int nb)
{
	unsigned int u_nb;

	u_nb = nb;
	if (nb < 0)
	{
		u_nb *= -1;
		write(1, "-", 1);
	}
	else if (nb == 0)
		write(1, "00", 2);
	else if (nb < 10)
		write(1, "0", 1);
	ft_nextchar(u_nb);
}

void	ft_print_comb2(void)
{
	int i;
	int j;

	i = 0;
	j = 1;
	while (i <= 98)
	{
		while (j <= 99)
		{
			ft_putnbr2(i);
			write(1, " ", 1);
			ft_putnbr2(j);
			if (i != 98)
				write(1, ", ", 2);
			j++;
		}
		i++;
		j = i + 1;
	}
}
